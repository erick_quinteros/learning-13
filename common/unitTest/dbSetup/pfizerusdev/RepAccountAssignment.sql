CREATE TABLE `RepAccountAssignment` (
  `repId` int(11) NOT NULL,
  `accountId` int(11) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`accountId`,`startDate`,`endDate`),
  KEY `repAccountAssignment_repId` (`repId`),
  KEY `repAccountAssignment_accountId` (`accountId`),
  KEY `repAccountAssignment_startDate` (`startDate`),
  KEY `repAccountAssignment_endDate` (`endDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci