library(RMySQL)

# read args passed from command line
args <- commandArgs(T)
if(length(args)==0){
  print("No arguments supplied.")
  if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}

# database connection config
dbuser <<- 'pfizerusadmin'
dbpassword <<- 'njTB95MacVDLaMUAnKHdXubOkVPyCU'
dbhost <<- '127.0.0.1'
port <<- 33066

# function to read SQL individually for later single use
extractMockData <- function(con,db,file,dirpath,saveDirpath) {
  filepath <- paste(dirpath,'/',file,sep='')
  tableName <- sub('\\.sql$', '', file)
  # read create table sql
  ff <- file(filepath,'r')
  SQL <- paste(readLines(ff, warn=FALSE),collapse='')
  close(ff)
  # read data
  data <- dbGetQuery(con, SQL)
  # write data to file
  saveFileName <- paste(saveDirpath,'/',sub('\\.sql$', '.csv', file),sep='')
  write.csv(data, file=saveFileName, row.names=FALSE)
  print(sprintf('finish extract mock data from Table: %s',tableName))
}

# funcation to extract data from a single table (mainly local set up)
extractOne <- function(filepath, dbuser, dbhost, port, password) {
  drv <- dbDriver("MySQL")
  # get filename, dbname,dirpath, saveDirpath
  file <- basename(filepath)
  db <- basename(dirname(filepath))
  dirpath <- dirname(filepath)
  saveDirpath <- paste(dirname(dirname(dirpath)),'data',db,sep='/')
  if (! dir.exists(saveDirpath)) {
    dir.create(saveDirpath)
  }
  con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=db,port=port,password=dbpassword)
  extractMockData(con,db,file,dirpath,saveDirpath)
  dbDisconnect(con)
}
# usage: 
# filepath <- '/Users/jiajingxu/Documents/learning/common/unitTest/mockDataExtract/pfizerusdev/Account.sql'
# extractOne(filepath, dbuser, dbhost, port, password)

# function to execute all extract SQL in one dir
extractAll <- function(homedir, testdir) {
  drv <- dbDriver("MySQL")
  # database list to be rewritten
  dir_list <- list.dirs(path=paste(homedir,'/',testdir, '/mockDataExtract',sep=''),full.names=FALSE,recursive=FALSE)
  # loop through list
  for (db in dir_list) {
    # create mock data folder '/data/' not already exists
    saveDirpath <- paste(homedir,'/',testdir,'/data/',db,sep='')
    if (! dir.exists(saveDirpath)) {
      dir.create(saveDirpath)
    }
    con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=db,port=port,password=dbpassword)
    dirpath <- paste(homedir,'/',testdir,'/mockDataExtract/',db,sep='')
    filesToRead <- list.files(path=dirpath)
    for (file in filesToRead) {
      extractMockData(con,db,file,dirpath,saveDirpath)
    }
    dbDisconnect(con)
  }
}

# run
if (! exists('testdir')) {
  testdir <- 'common/unitTest'
}

extractAll(homedir,testdir)

# further modify the mock data loaded from DB
# pfizerusdev_stage.Sent_Email_vod__c_arc
sentEmailVodCArc <- read.csv(file=sprintf('%s/common/unitTest/data/pfizerusdev_stage/Sent_Email_vod__c_arc.csv', homedir), header=TRUE, sep=",",as.is=TRUE)
sentEmailVodCArc$Status_vod__c <- 'Delivered_vod'
sentEmailVodCArc$Email_Config_Values_vod__c <- NA
selectedInteraction <- read.csv(file=sprintf('%s/common/unitTest/data/pfizerusdev/Interaction.csv', homedir), header=TRUE, sep=",",as.is=TRUE)
selectedInteractionId <- as.vector(selectedInteraction[selectedInteraction$interactionTypeId==11,c("externalId")])
sentEmailVodCArc <- sentEmailVodCArc[rep(1, length(selectedInteractionId)), ]
sentEmailVodCArc$Id <- selectedInteractionId
write.csv(sentEmailVodCArc, file=sprintf('%s/common/unitTest/data/pfizerusdev_stage/Sent_Email_vod__c_arc.csv', homedir), row.names=FALSE)
# pfizerusdev.MessageSet
messageSet <- read.csv(file=sprintf('%s/common/unitTest/data/pfizerusdev/MessageSet.csv', homedir), header=TRUE, sep=",", as.is=TRUE)
messageSet[1,c("messageSetId","messageSetName","messageSetDescription","messageAlgorithmId")] <- c(2001,"unit-test-chantix-1","include msg a3RA00000001MtAMAU, a3RA0000000e47gMAA, a3RA0000000e486MAA (703,49,10)",23)
messageSet[2,c("messageSetId","messageSetName","messageSetDescription","messageAlgorithmId")] <- c(2002,"unit-test-chantix-2","include msg a3RA00000001MtAMAU (703)",NA)
messageSet[3,c("messageSetId","messageSetName","messageSetDescription","messageAlgorithmId")] <- c(2003,"unit-test-chantix-1-copy1","include msg a3RA00000001MtAMAU, a3RA0000000e47gMAA, a3RA0000000e486MAA (703,49,10)",23)
messageSet[4,c("messageSetId","messageSetName","messageSetDescription","messageAlgorithmId")] <- c(2004,"unit-test-chantix-1-copy2","include msg a3RA00000001MtAMAU, a3RA0000000e47gMAA, a3RA0000000e486MAA (703,49,10)",25)
write.csv(messageSet, file=sprintf('%s/common/unitTest/data/pfizerusdev/MessageSet.csv', homedir), row.names=FALSE)
# pfizerusdev.MessageSetMessage
messageSetMessage <- read.csv(file=sprintf('%s/common/unitTest/data/pfizerusdev/MessageSetMessage.csv', homedir), header=TRUE, sep=",",as.is=TRUE)
messageSetMessage[,c("messageSetId","messageId","sequenceOrder")] <- list(c(2001,2001,2001,2002,2003,2003,2003,2004,2004,2004),c(703,49,10,703,703,49,10,703,49,10),c(1,2,3,1,2,3,1,1,2,3))
write.csv(messageSetMessage, file=sprintf('%s/common/unitTest/data/pfizerusdev/MessageSetMessage.csv', homedir), row.names=FALSE)
# pfizerusdev.MessageAlogrithm
messageAlgorithm <- read.csv(file=sprintf('%s/common/unitTest/data/pfizerusdev/MessageAlgorithm.csv', homedir), header=TRUE, sep=",", as.is=TRUE)
messageAlgorithm[1,c("messageAlgorithmId","externalId","messageAlgorithmDescription","isActive","isDeleted")] <- c(23,"00bad2df-bd27-48da-a37c-0e3b41e949e1","unit-test-chantix-config",1,0)
messageAlgorithm[2,c("isActive","isDeleted")] <- c(0,1)
messageAlgorithm[3,c("messageAlgorithmId","externalId","messageAlgorithmDescription","isActive","isDeleted")] <- c(25,"unit-test-mso-build-copy-configUID","unit-test-chantix-config-copy",1,0)
write.csv(messageAlgorithm, file=sprintf('%s/common/unitTest/data/pfizerusdev/MessageAlgorithm.csv', homedir), row.names=FALSE)


# Rscript common/unitTest/mockDataExtract.R homedir="'"'/Users/jiajingxu/Documents/learning'"'"