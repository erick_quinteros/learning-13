initializeSpark <- function(homedir, master="local", version="2.3.1", conf=NULL) {
  if (is.null(conf)) {
    conf <- spark_config()
    if (master=="local") {
      conf$`sparklyr.shell.driver-memory` <- "12G"
    } else if (master=="yarn-client") {
      conf$spark.driver.memory <- "45G"
      conf$spark.sql.shuffle.partitions <- 48
      conf$spark.rpc.message.maxSize <- 1350
      conf$spark.dynamicAllocation.executorIdleTimeout <- "300s"
      conf$spark.dynamicAllocation.minExecutors <- 1
    } else if (master=="yarn-cluster") {
      conf$spark.executor.cores <- 5
      conf$spark.executor.memory <- "10G"
      conf$spark.dynamicAllocation.enabled <- "true"
      conf$spark.dynamicAllocation.maxExecutors <- 11
      conf$spark.rpc.message.maxSize <- 1350
    }
  }
  jars.path <- list.files(paste(homedir,"common/sparkUtils/jars",sep="/"), full.names=TRUE)
  conf$`sparklyr.shell.jars` <- jars.path
  sc <- spark_connect(master=master, version=version, config=conf)
  return(sc)
}

