context('testing sparkBuildStaticDesignMatrix from learningPackage')
print(Sys.time())

# load packages and scripts needed for running tests
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))
library(sparklyr)
library(arrow)

# loading Learning package to call function for test
library(sparkLearning)

# load required input
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountProduct.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
prods <- strsplit(readModuleConfig(homedir, 'common/unitTest','prods'),";")[[1]]

# convert saved data to spark object
sc <- initializeSpark(homedir)
accountProduct <- sdf_copy_to(sc, accountProduct, "accountProduct", overwrite=TRUE, memory=TRUE)

# set up log file to test log contents
fileName <- sprintf('%s/common/unitTest/tests/log_sparkBuildStaticDesignMatrix.txt',homedir)
if (file.exists(fileName)) file.remove(fileName)
flog.appender(appender.file(fileName))

# test fucntion with logprocess logDropProcess=TRUE and removeSpecialCharacterInAP=TRUE (no need to test for FALSE, as \n is always removed)
result <- sparkBuildStaticDesignMatrix(prods, accountProduct, emailTopicNames, logDropProcess=TRUE)

test_that("result has the correct dimension and are the same as saved for removeSpecialCharacterInAP=TRUE", {
  expect_length(result,3)
  expect_equal(sdf_dim(result[["AP"]]), c(309,606))
  expect_equal(dim(result[["APpredictors"]]), c(204,3))
  expect_length(result[[3]], 533)
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_AP.RData',homedir))
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_APpredictors.RData',homedir))
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_predictorNamesAPColMap.RData',homedir))
  AP_new <- convertSDFToDF(result[["AP"]])
  # setnames(AP_new, old=as.character(predictorNamesAPColMap), new=names(predictorNamesAPColMap))
  setkey(AP_new, accountId)
  setkey(AP, accountId)
  expect_equal(AP_new, AP)
  expect_equal(result[["APpredictors"]], APpredictors)
  expect_equal(result[["predictorNamesAPColMap"]], predictorNamesAPColMap)
})

test_that("check log file contents", {
  log_contents <- readLogFileBase(fileName)
  expect_str_start_match(log_contents[3],'151 of 204')  # number of predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value
  expect_str_start_match(log_contents[4],'1 of 21 numeric predictors (53 all predictors)')  # number of numeric predictors from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0
  expect_str_start_match(log_contents[5],'0 of 52')  # number of remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately
  expect_str_end_match(log_contents[6],'52 to 533')  # check for dcast manupilation
  expect_str_end_match(log_contents[7],'added 72 and becomes 605') # check EmailTopic Open Rates predictor added
})

flog.appender(appender.console())
file.remove(fileName)
closeSpark(sc)