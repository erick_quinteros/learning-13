FROM databricksruntime/standard:latest
LABEL dev1="sundeep.nukaraju@aktana.com" dev2="sai.swaroop@aktana.com"

ENV MLFLOW_TRACKING_URI=databricks
ENV DATABRICKS_HOST=https://aktana-poc.cloud.databricks.com
ENV DATABRICKS_TOKEN=dapidc5f8401da1715a24d5d592ce62c46ef

RUN /databricks/conda/envs/dcs-minimal/bin/pip install  mlflow>=1.0 \
    snowflake-connector-python

RUN mkdir /learning
COPY . /learning
