INFO [2018-07-23 16:52:33] Run directory = /Users/jiajingxu/Documents/learning/bin/../builds/unit-test-rem
INFO [2018-07-23 16:52:33] useForProbability is T
INFO [2018-07-23 16:52:34] read Interaction table
INFO [2018-07-23 16:52:34] read InteractionAccount table
INFO [2018-07-23 16:52:34] read Account table
INFO [2018-07-23 16:52:34] read Rep table
INFO [2018-07-23 16:52:34] read repAssignments table
INFO [2018-07-23 16:52:34] read stage db Suggestion_Feedback_vod__c_arc table
INFO [2018-07-23 16:52:34] read stage db RecordType_vod__c_arc table
INFO [2018-07-23 16:52:34] read stage db Suggestion_vod__c_arc table
INFO [2018-07-23 16:52:34] merge suggestions and feedback
INFO [2018-07-23 16:52:34] Done with loadEngagementData.
INFO [2018-07-23 16:52:34] calculateEngagement for channel 3 ...
INFO [2018-07-23 16:52:34] Now prepare for calculation of likelihood in parallel, using 4 cores
INFO [2018-07-23 16:52:34] Done with calculating probTouch using events/interactions. Now estimates the likelihood of an interaction by day of week and week of month.
INFO [2018-07-23 16:52:34] Get final likelihood.
INFO [2018-07-23 16:52:34] calculateTriggerEngagement for channel 3 ...
INFO [2018-07-23 16:52:34] calculateEngagement for channel 12 ...
INFO [2018-07-23 16:52:34] Now prepare for calculation of likelihood in parallel, using 4 cores
INFO [2018-07-23 16:52:34] Done with calculating probTouch using events/interactions. Now estimates the likelihood of an interaction by day of week and week of month.
INFO [2018-07-23 16:52:34] Get final likelihood.
INFO [2018-07-23 16:52:34] calculateTriggerEngagement for channel 12 ...
INFO [2018-07-23 16:52:34] Now saveEngagementResult ...
INFO [2018-07-23 16:52:34] Rows in engagement result: 23
INFO [2018-07-23 16:52:35] INSERT INTO LearningRun SQL is: INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,executionStatus,executionDateTime) VALUES('8b80dea3-15c5-4fdc-975b-a1e830baed9e','unit-test-rem','ab480b48-8106-4ff5-a063-68b77f3ba094','AKT_REM_V0',1,'success','2018-07-23 16:52:35');
INFO [2018-07-23 16:52:35] update LearningRun SQL is: UPDATE LearningRun SET executionDateTime='2018-07-23 16:52:35', executionStatus='success' WHERE learningRunUID='8b80dea3-15c5-4fdc-975b-a1e830baed9e';
