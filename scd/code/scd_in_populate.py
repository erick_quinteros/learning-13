# Databricks notebook source
# DBTITLE 1,SCD_IN Global Parameters Initialization
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql.types import *
from decimal import Decimal
from pyspark.sql import Window
import snowflake.connector
import pyspark.sql.functions as F
import logging
import sys

logger = logging.getLogger("SCD_IN_POPULATE")
logger.info("Initializing Global Parameters")
is_exists_F_BRICK_WEEKLY = True
is_exists_F_BRICK_MONTHLY = True
is_exists_F_ACCOUNT_WEEKLY = True
is_exists_F_ACCOUNT_MONTHLY = True
is_exists_DIM_FREQUENCY = True
is_brick = False
is_account = False

if len(sys.argv) != 7:
    logger.error("Invalid arguments to the SCD_IN_POPULATE script")
    exit(0)

user = sys.argv[1]
password = sys.argv[2]
account = sys.argv[3]
warehouse = sys.argv[4]
database = sys.argv[5]
schemaname = sys.argv[6]

options = {
    "sfUrl": "{}.snowflakecomputing.com/".format(account),
    "sfUser": user,
    "sfPassword": password,
    "sfDatabase": database,
    "sfSchema": schemaname,
    "sfWarehouse": warehouse,
}

# connection = snowflake.connector.connect(account=account, user=user,password=password,warehouse=warehouse,database=database,schema=schema)
spark = SparkSession.builder.appName("SCD_IN Populate Tables ").getOrCreate()
sqlContext = SQLContext(sparkContext=spark.sparkContext, sparkSession=spark)

schema = StructType([])
empty = sqlContext.createDataFrame(spark.sparkContext.emptyRDD(), schema)
df_BRICK_WEEKLY = empty
df_BRICK_MONTHLY = empty
df_ACCOUNT_WEEKLY = empty
df_ACCOUNT_MONTHLY = empty
df_DIM_FREQUENCY = empty
df_BRICK = empty
df_ACCOUNT = empty
timeFmt = "yyyy-MM-dd' 'HH:mm:ss.SSS"

logger.info("Completed initializing Global Parameters")


# COMMAND ----------

# DBTITLE 1,Get MAX of DSE_PRODUCT and EXTERNAL_PRODUCT
def get_max_productkey_df(df):
    def max_function(*cols):
        return max(x for x in cols if x is not None)

    max_func_udf = F.udf(max_function, DecimalType())
    df_maxProduct = df.withColumn('PRODUCT_KEY', max_func_udf('DSE_PRODUCT_KEY', 'EXTERNAL_PRODUCT_KEY'))
    return df_maxProduct


# COMMAND ----------

# DBTITLE 1,Generate Primary Key Column
def add_primary_key_incremental(df, useColumn, addColumn):
    w = Window().orderBy(useColumn)
    df_final = df.withColumn(addColumn, F.row_number().over(w).cast("integer"))
    return df_final


# COMMAND ----------

# DBTITLE 1,Add Columns IS_ACTIVE, CREATED_TS, UPDATED_TS
def add_default_columns(df):
    df_addCols = df.withColumn("IS_ACTIVE", F.lit(True))
    df_addCols = df_addCols.withColumn("CREATED_TS",
                                       F.date_format(F.current_timestamp(), timeFmt).cast(TimestampType()))
    df_addCols_final = df_addCols.withColumn("UPDATED_TS",
                                             F.date_format(F.current_timestamp(), timeFmt).cast(TimestampType()))
    return df_addCols_final


# COMMAND ----------

# DBTITLE 1,Merge Brick Weekly and Monthly Tables
def merge_brick_weekly_monthly(df_BRICK_WEEKLY, df_BRICK_MONTHLY):
    if is_exists_F_BRICK_WEEKLY == True and is_exists_F_BRICK_MONTHLY == True:
        df = df_BRICK_WEEKLY.union(df_BRICK_MONTHLY)
    elif is_exists_F_BRICK_WEEKLY == True and is_exists_F_BRICK_MONTHLY == False:
        df = df_BRICK_WEEKLY
    elif is_exists_F_BRICK_WEEKLY == False and is_exists_F_BRICK_MONTHLY == True:
        df = df_BRICK_MONTHLY
    df = df.distinct()
    return df


# COMMAND ----------

# DBTITLE 1,Merge Account Weekly and Monthly Tables
def merge_account_weekly_monthly(df_ACCOUNT_WEEKLY, df_ACCOUNT_MONTHLY):
    if is_exists_F_ACCOUNT_WEEKLY == True and is_exists_F_ACCOUNT_MONTHLY == True:
        df = df_ACCOUNT_WEEKLY.union(df_ACCOUNT_MONTHLY)
    elif is_exists_F_ACCOUNT_WEEKLY == True and is_exists_F_ACCOUNT_MONTHLY == False:
        df = df_ACCOUNT_WEEKLY
    elif is_exists_F_ACCOUNT_WEEKLY == False and is_exists_F_ACCOUNT_MONTHLY == True:
        df = df_ACCOUNT_MONTHLY
    df = df.distinct()
    return df


# COMMAND ----------

# DBTITLE 1,Calculate Product Frequency Table
def calculate_product_frequency():
    df = empty
    if is_brick == True and is_account == False:
        df_brick_product_frequency = df_BRICK.select('PRODUCT_KEY', 'FREQUENCY_KEY')
        df = df_brick_product_frequency.distinct()
    if is_brick == False and is_account == True:
        df_account_product_frequency = df_ACCOUNT.select('PRODUCT_KEY', 'FREQUENCY_KEY')
        df = df_account_product_frequency.distinct()
    if is_brick == True and is_account == True:
        df_brick_product_frequency = df_BRICK.select('PRODUCT_KEY', 'FREQUENCY_KEY')
        df_account_product_frequency = df_ACCOUNT.select('PRODUCT_KEY', 'FREQUENCY_KEY')
        df = df_brick_product_frequency.union(df_account_product_frequency)
        df = df.distinct()
    df_add_key = add_primary_key_incremental(df, 'PRODUCT_KEY', 'PRODUCT_FREQUENCY_KEY')
    df_PRODUCT_FREQUENCY_final = add_default_columns(df_add_key)
    return df_PRODUCT_FREQUENCY_final.select('PRODUCT_FREQUENCY_KEY', 'PRODUCT_KEY', 'FREQUENCY_KEY', 'IS_ACTIVE',
                                             'CREATED_TS', 'UPDATED_TS')


# COMMAND ----------

# DBTITLE 1,Calculate Metric Calc Name Table
def calculate_metric_calc_name():
    columns = ['METRIC_CALC_NAME_KEY', 'METRIC_CALC_NAME', 'METRIC_CALC_DESC']
    vals = [(1, 'SALES VOLUME', 'SALES VOLUME'), (2, 'ORDER GAP', 'ORDER GAP'),
            (3, 'MARKETBASKET VOLUME', 'MARKETBASKET VOLUME'), (4, 'MARKET SHARE', 'MARKET SHARE')]
    df = spark.createDataFrame(vals, columns)
    df = df.orderBy("METRIC_CALC_NAME_KEY")
    df = add_default_columns(df)
    return df.select('METRIC_CALC_NAME_KEY', 'METRIC_CALC_NAME', 'METRIC_CALC_DESC', 'IS_ACTIVE', 'CREATED_TS',
                     'UPDATED_TS')


# COMMAND ----------

# DBTITLE 1,Calculate Product Metric Calc Table
def calculate_product_metric_calc(df_PRODUCT_FREQUENCY, df_METRIC_CALC_NAME):
    df = df_PRODUCT_FREQUENCY.alias("a").crossJoin(df_METRIC_CALC_NAME.alias("b")).select("a.PRODUCT_FREQUENCY_KEY",
                                                                                          "a.PRODUCT_KEY",
                                                                                          "b.METRIC_CALC_NAME_KEY")
    df = df.sort("PRODUCT_FREQUENCY_KEY")
    df = add_primary_key_incremental(df, 'PRODUCT_FREQUENCY_KEY', 'PRODUCT_METRIC_CALC_KEY')
    df = df.withColumn("USE_CUSTOM_CALC", F.lit(False))
    df = add_default_columns(df)
    return df.select('PRODUCT_METRIC_CALC_KEY', 'PRODUCT_FREQUENCY_KEY', 'PRODUCT_KEY', 'METRIC_CALC_NAME_KEY',
                     'USE_CUSTOM_CALC', 'IS_ACTIVE', 'CREATED_TS', 'UPDATED_TS')


# COMMAND ----------

# DBTITLE 1,Calculate Product Metric Period Table
def calculate_product_metric_period(df_PRODUCT_METRIC_CALC):
    df = empty
    if is_brick == True and is_account == False:
        df = df_PRODUCT_METRIC_CALC.alias("a").join(df_BRICK.alias("b"), on="PRODUCT_KEY").select(
            "a.PRODUCT_METRIC_CALC_KEY", "a.PRODUCT_KEY", "B.METRIC_KEY")
        df = df.withColumn("REPORTING_LEVEL_KEY", F.lit(Decimal(1)))
    if is_brick == False and is_account == True:
        df = df_PRODUCT_METRIC_CALC.alias("a").join(df_ACCOUNT.alias("b"), on="PRODUCT_KEY").select(
            "a.PRODUCT_METRIC_CALC_KEY", "a.PRODUCT_KEY", "B.METRIC_KEY")
        df = df.withColumn("REPORTING_LEVEL_KEY", F.lit(Decimal(2)))
    if is_brick == True and is_account == True:
        df_brick_product_metric_calc = df_PRODUCT_METRIC_CALC.alias("a").join(df_BRICK.alias("b"),
                                                                              on="PRODUCT_KEY").select(
            "a.PRODUCT_METRIC_CALC_KEY", "a.PRODUCT_KEY", "B.METRIC_KEY")
        df_brick_product_metric_calc = df_brick_product_metric_calc.withColumn("REPORTING_LEVEL_KEY", F.lit(Decimal(1)))
        df_account_product_metric_calc = df_PRODUCT_METRIC_CALC.alias("a").join(df_ACCOUNT.alias("b"),
                                                                                on="PRODUCT_KEY").select(
            "a.PRODUCT_METRIC_CALC_KEY", "a.PRODUCT_KEY", "B.METRIC_KEY")
        df_account_product_metric_calc = df_account_product_metric_calc.withColumn("REPORTING_LEVEL_KEY",
                                                                                   F.lit(Decimal(2)))
        df = df_brick_product_metric_calc.union(df_account_product_metric_calc)
    df = df.distinct()
    df = df.orderBy(["PRODUCT_METRIC_CALC_KEY", "METRIC_KEY"], ascending=True)
    df = add_primary_key_incremental(df, 'PRODUCT_METRIC_CALC_KEY', 'PRODUCT_METRIC_PERIOD_KEY')
    df = df.withColumn("MINIMUM_PERIOD_NUMBER", F.lit(Decimal(1)))
    df = df.withColumn("MAXIMUM_PERIOD_NUMBER", F.lit(Decimal(13)))
    df = add_default_columns(df)
    return df.select('PRODUCT_METRIC_PERIOD_KEY', 'PRODUCT_METRIC_CALC_KEY', 'METRIC_KEY', 'PRODUCT_KEY',
                     'MINIMUM_PERIOD_NUMBER', 'MAXIMUM_PERIOD_NUMBER', 'IS_ACTIVE', 'CREATED_TS', 'UPDATED_TS',
                     'REPORTING_LEVEL_KEY')


# COMMAND ----------

# DBTITLE 1,Calculate Reporting Level Table
def calculate_reporting_level():
    columns = ['REPORTING_LEVEL_KEY', 'REPORTING_LEVEL_NAME']
    vals = [(1, 'BRICK'), (2, 'ACCOUNT')]
    df = spark.createDataFrame(vals, columns)
    df = add_default_columns(df)
    return df.select('REPORTING_LEVEL_KEY', 'REPORTING_LEVEL_NAME', 'IS_ACTIVE', 'CREATED_TS', 'UPDATED_TS')


# COMMAND ----------

# DBTITLE 1,Calculate Product Metric Level Table
def calcualate_product_metric_level(df_PRODUCT_METRIC_PERIOD):
    df = df_PRODUCT_METRIC_PERIOD.select("PRODUCT_METRIC_PERIOD_KEY", "REPORTING_LEVEL_KEY")
    df = df.orderBy("PRODUCT_METRIC_PERIOD_KEY")
    df = add_default_columns(df)
    df = add_primary_key_incremental(df, 'PRODUCT_METRIC_PERIOD_KEY', 'PRODUCT_METRIC_PERIOD_LEVEL_KEY')
    return df.select('PRODUCT_METRIC_PERIOD_LEVEL_KEY', 'PRODUCT_METRIC_PERIOD_KEY', 'REPORTING_LEVEL_KEY', 'IS_ACTIVE',
                     'CREATED_TS', 'UPDATED_TS')


# COMMAND ----------

# DBTITLE 1,Format Table View Name
def target_view_name(DATATYPE, FREQUENCY, VIEWNAME):
    return 'vw_' + DATATYPE + '_' + FREQUENCY + '_' + VIEWNAME.replace(" ", "_")


# COMMAND ----------

# DBTITLE 1,Get FREQUENCY_KEY and TARGET_VIEW_NAME for Target View Mapping Table
def calculate_target_view_name_frequency(df_REPORTING_LEVEL, df_METRIC_CALC_NAME_KEY, datatype, frequency):
    taget_view_name_udf = F.udf(target_view_name, StringType())
    df_REPORTING_LEVEL_KEY = df_REPORTING_LEVEL.filter(df_REPORTING_LEVEL["REPORTING_LEVEL_NAME"] == datatype).select(
        "REPORTING_LEVEL_KEY")
    FREQUENCY_KEY = \
    df_DIM_FREQUENCY.filter(df_DIM_FREQUENCY["FREQUENCY_NAME"] == frequency).select("FREQUENCY_KEY").collect()[0][0]
    df = df_METRIC_CALC_NAME_KEY.crossJoin(df_REPORTING_LEVEL_KEY)
    df = df.withColumn("FREQUENCY_KEY", F.lit(FREQUENCY_KEY))
    df = df.withColumn("TARGET_VIEW_NAME",
                       F.lit(taget_view_name_udf(F.lit("BRICK"), F.lit(frequency.upper()), df["METRIC_CALC_NAME"])))
    return df


# COMMAND ----------

# DBTITLE 1,Populate Target View Mapping Table
def calculate_target_view_mapping(df_METRIC_CALC_NAME, df_REPORTING_LEVEL):
    schema = StructType(
        [StructField('METRIC_CALC_NAME_KEY', IntegerType(), True), StructField('METRIC_CALC_NAME', StringType(), True),
         StructField('REPORTING_LEVEL_KEY', IntegerType(), True), StructField('FREQUENCY_KEY', IntegerType(), True),
         StructField('TARGET_VIEW_NAME', StringType(), True)])
    df = spark.createDataFrame([], schema)
    df_METRIC_CALC_NAME_KEY = df_METRIC_CALC_NAME.select("METRIC_CALC_NAME_KEY", "METRIC_CALC_NAME")
    if is_exists_DIM_FREQUENCY:
        if is_exists_F_BRICK_WEEKLY:
            df1 = calculate_target_view_name_frequency(df_REPORTING_LEVEL, df_METRIC_CALC_NAME_KEY, "BRICK", "Weekly")
            df = df.union(df1)
        if is_exists_F_BRICK_MONTHLY:
            df2 = calculate_target_view_name_frequency(df_REPORTING_LEVEL, df_METRIC_CALC_NAME_KEY, "BRICK", "Monthly")
            df = df.union(df2)
        if is_exists_F_ACCOUNT_WEEKLY:
            df3 = calculate_target_view_name_frequency(df_REPORTING_LEVEL, df_METRIC_CALC_NAME_KEY, "ACCOUNT", "Weekly")
            df = df.union(df3)
        if is_exists_F_ACCOUNT_MONTHLY:
            df4 = calculate_target_view_name_frequency(df_REPORTING_LEVEL, df_METRIC_CALC_NAME_KEY, "ACCOUNT",
                                                       "Monthly")
            df = df.union(df4)
        df = df.withColumn("IS_CALC_CUSTOM", F.lit(False))
        df = add_default_columns(df)
        df = add_primary_key_incremental(df, 'METRIC_CALC_NAME_KEY', 'TARGET_VIEW_MAPPING_KEY')
    else:
        print("DIM_FREQUENCY doesn't exists")
        columns = ['TARGET_VIEW_MAPPING_KEY', 'METRIC_CALC_NAME_KEY', 'TARGET_VIEW_NAME', 'REPORTING_LEVEL_KEY',
                   'FREQUENCY_KEY', 'IS_CALC_CUSTOM', 'IS_ACTIVE', 'CREATED_TS', 'UPDATED_TS']
        vals = []
        df = spark.createDataFrame(vals, columns)
    return df.select('TARGET_VIEW_MAPPING_KEY', 'METRIC_CALC_NAME_KEY', 'TARGET_VIEW_NAME', 'REPORTING_LEVEL_KEY',
                     'FREQUENCY_KEY', 'IS_CALC_CUSTOM', 'IS_ACTIVE', 'CREATED_TS', 'UPDATED_TS')


# COMMAND ----------

# DBTITLE 1,Truncate Table Function
def truncate_table(tablename):
    connection = snowflake.connector.connect(account=account, user=user, password=password, warehouse=warehouse,
                                             database=database, schema=schemaname)
    cursor = connection.cursor()
    try:
        query = "truncate table {};".format(tablename)
        cursor.execute(query)
    except Exception as e:
        raise e
    finally:
        cursor.close()
    connection.close()


# COMMAND ----------

# DBTITLE 1,Read Table Data From Snowflake
def read_data_from_table(tablename):
    logger.info("Trying to fetch table {}".format(tablename))
    df = empty
    statement = "select DSE_PRODUCT_KEY, EXTERNAL_PRODUCT_KEY, METRIC_KEY, FREQUENCY_KEY  from {}".format(tablename)
    df = spark.read.format("snowflake").options(**options).option("query", statement).load()
    try:
        df = get_max_productkey_df(df).select('PRODUCT_KEY', 'METRIC_KEY', 'FREQUENCY_KEY').distinct()
    except Exception as e:
        raise e
    return df


# COMMAND ----------

try:
    df_BRICK_WEEKLY = read_data_from_table('F_BRICK_WEEKLY')
except Exception:
    is_exists_F_BRICK_WEEKLY = False
    logger.info("Snowflake Connection issue or F_BRICK_WEEKLY table doesn't exists")

try:
    df_BRICK_MONTHLY = read_data_from_table('F_BRICK_MONTHLY')
except Exception:
    is_exists_F_BRICK_MONTHLY = False
    logger.info("Snowflake Connection issue or F_BRICK_MONTHLY table doesn't exists")

try:
    df_ACCOUNT_WEEKLY = read_data_from_table('F_ACCOUNT_WEEKLY')
except Exception:
    is_exists_F_ACCOUNT_WEEKLY = False
    logger.info("Snowflake Connection issue or F_ACCOUNT_WEEKLY table doesn't exists")

try:
    df_ACCOUNT_MONTHLY = read_data_from_table('F_ACCOUNT_MONTHLY')
except Exception:
    is_exists_F_ACCOUNT_MONTHLY = False
    logger.info("Snowflake Connection issue or F_ACCOUNT_MONTHLY table doesn't exists")

try:
    df_DIM_FREQUENCY = spark.read.format("snowflake").options(**options).option("dbtable", "DIM_FREQUENCY").load()
except Exception:
    is_exists_DIM_FREQUENCY = False
    logger.info("Snowflake Connection issue or DIM_FREQUENCY table doesn't exists")

# COMMAND ----------

logger.info("Start processing data for SCD_IN Tables")

df_METRIC_CALC_NAME = calculate_metric_calc_name()

df_REPORTING_LEVEL = calculate_reporting_level()

df_TARGET_VIEW_MAPPING = calculate_target_view_mapping(df_METRIC_CALC_NAME, df_REPORTING_LEVEL)

if is_exists_F_BRICK_WEEKLY or is_exists_F_BRICK_MONTHLY:
    logger.info("Brick tables exists, setting is_brick to true")
    is_brick = True
    df_BRICK = merge_brick_weekly_monthly(df_BRICK_WEEKLY, df_BRICK_MONTHLY)  # Merging weekly and monthly Brick tables

if is_exists_F_ACCOUNT_WEEKLY or is_exists_F_ACCOUNT_MONTHLY:
    logger.info("Account tables exists, setting is_account to true")
    is_account = True
    df_ACCOUNT = merge_account_weekly_monthly(df_ACCOUNT_WEEKLY, df_ACCOUNT_MONTHLY)  # Merging weekly and monthly Brick tables

df_PRODUCT_FREQUENCY = calculate_product_frequency()

df_PRODUCT_METRIC_CALC = calculate_product_metric_calc(df_PRODUCT_FREQUENCY, df_METRIC_CALC_NAME)

df_PRODUCT_METRIC_PERIOD = calculate_product_metric_period(df_PRODUCT_METRIC_CALC)

df_PRODUCT_METRIC_PERIOD_LEVEL = calcualate_product_metric_level(df_PRODUCT_METRIC_PERIOD)


# COMMAND ----------

logger.info("Truncate SCD_IN_METRIC_CALC_NAME")
truncate_table("SCD_IN_METRIC_CALC_NAME")
logger.info("Populate SCD_IN_METRIC_CALC_NAME")
df_METRIC_CALC_NAME.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_METRIC_CALC_NAME").mode("append").save()

logger.info("Truncate SCD_IN_REPORTING_LEVEL")
truncate_table("SCD_IN_REPORTING_LEVEL")
logger.info("Populate SCD_IN_REPORTING_LEVEL")
df_REPORTING_LEVEL.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_REPORTING_LEVEL").mode("append").save()

logger.info("Truncate SCD_IN_TARGET_VIEW_MAPPING")
truncate_table("SCD_IN_TARGET_VIEW_MAPPING")
logger.info("Populate SCD_IN_TARGET_VIEW_MAPPING")
df_TARGET_VIEW_MAPPING.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_TARGET_VIEW_MAPPING").mode("append").save()

logger.info("Truncate SCD_IN_PRODUCT_FREQUENCY")
truncate_table("SCD_IN_PRODUCT_FREQUENCY")
logger.info("Populate SCD_IN_PRODUCT_FREQUENCY")
df_PRODUCT_FREQUENCY.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_PRODUCT_FREQUENCY").mode("append").save()

logger.info("Truncate SCD_IN_PRODUCT_METRIC_CALC")
truncate_table("SCD_IN_PRODUCT_METRIC_CALC")
logger.info("Populate SCD_IN_PRODUCT_METRIC_CALC")
df_PRODUCT_METRIC_CALC.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_PRODUCT_METRIC_CALC").mode("append").save()

logger.info("Truncate SCD_IN_PRODUCT_METRIC_PERIOD")
truncate_table("SCD_IN_PRODUCT_METRIC_PERIOD")
logger.info("Drop column SCD_IN_PRODUCT_METRIC_PERIOD")
df_PRODUCT_METRIC_PERIOD = df_PRODUCT_METRIC_PERIOD.drop('REPORTING_LEVEL_KEY')
logger.info("Populate SCD_IN_PRODUCT_METRIC_PERIOD")
df_PRODUCT_METRIC_PERIOD.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_PRODUCT_METRIC_PERIOD").mode("append").save()

logger.info("Truncate SCD_IN_PRODUCT_METRIC_PERIOD_LEVEL")
truncate_table("SCD_IN_PRODUCT_METRIC_PERIOD_LEVEL")
logger.info("Populate SCD_IN_PRODUCT_METRIC_PERIOD_LEVEL")
df_PRODUCT_METRIC_PERIOD_LEVEL.write.format("snowflake").options(**options).option("dbtable", "SCD_IN_PRODUCT_METRIC_PERIOD_LEVEL").mode("append").save()

logger.info("Done populating SCD_IN Tables")
