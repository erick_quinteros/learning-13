context('testing paraRunNightlyDriver script in paraRun module')
print(Sys.time())

############################### func for run anchor setup to have paraRun ########################################
runAnchorDriverMockDataSetup <- function (isNightly,buildUID) {
  # load library and source script
  library(Learning)
  library(Learning.DataAccessLayer)
  library(uuid)
  source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
  source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
  source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('RepDateLocation','RepDateFacility'),pfizerusdev_learning=c('RepDateLocation','RepDateFacility','LearningRun','LearningBuild','AccountDateLocationScores','RepCalendarAdherence','RepAccountCalendarAdherence'))
  # learningBuild not required for run engagementDriver, just for testing
  requiredMockDataList_module <- list(pfizerusdev_learning=c('AccountDateLocation'), pfizerusdev=c('Interaction','Facility','Rep','InteractionType','InteractionAccount','RepTeamRep','AccountDateLocation','Account'), pfizerusdev_stage=c('RepAccountAssignment_arc'), pfizerusdev_archive=c('STG_Call2_vod__c_H'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')
  # set up unit test build folder and related configs
  # get buildUID
  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  if (!isNightly) {
    setupMockBuildDir(homedir, 'anchor', buildUID, files_to_copy='learning.properties', forNightly=FALSE)
  } else {
    setupMockBuildDir(homedir, 'anchor', buildUID, files_to_copy=NULL, forNightly=TRUE)
  }
}

############################### func for run anchorDriver ########################################
runParaRunDriverForAnchor <- function (isNightly, buildUID) {
  runmodel <<- "anchor"
  rundriver <<- "anchorDriver"

  if (!isNightly) {
    # manual key in rundate
    # rundate <<- readModuleConfig(homedir, 'anchor','rundate')
    # parameters needed for manual running
    RUN_UID <<- UUIDgenerate()
    BUILD_UID <<- buildUID
    config <- initializeConfigurationNew(homedir, BUILD_UID)
    versionUID <- config[["versionUID"]]
    configUID  <- config[["configUID"]]
    con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',0,'ANCHOR','running','%s');",RUN_UID,buildUID,versionUID,configUID,now)

    dbClearResult(dbSendQuery(con_l, SQL))
    dbDisconnect(con_l)
  } else {
    if (exists("BUILD_UID", envir=globalenv())) {rm(BUILD_UID, envir=globalenv())}
    if (exists("RUN_UID", envir=globalenv())) {rm(RUN_UID, envir=globalenv())}
  }
  set.seed(1)
  source(sprintf("%s/paraRun/paraRunDriver.R",homedir))

}


############################### main testing funcs ########################################

# test for nightly run
test_that('test for nightly running', {
  # run script
  isNightly <- TRUE
  buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
  runAnchorDriverMockDataSetup(isNightly, buildUID)
  runParaRunDriverForAnchor(isNightly, buildUID)

  # test whether writes to correct output tables
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  # old table should not have any value
  repDateLocation <- dbGetQuery(con, 'SELECT * from RepDateLocation;')
  repDateLocation_l <- dbGetQuery(con_l, 'SELECT * from RepDateLocation;')
  repDateFacility <- dbGetQuery(con, 'SELECT * from RepDateFacility;')
  repDateFacility_l <- dbGetQuery(con_l, 'SELECT * from RepDateFacility;')
  accountDateLocationScores <- dbGetQuery(con_l, 'SELECT * from AccountDateLocationScores;')
  accountDateLocation <- dbGetQuery(con, 'SELECT * from AccountDateLocation;')
  accountDateLocation_l <- dbGetQuery(con_l, 'SELECT * from AccountDateLocation;')
  repCalendarAdherence <- dbGetQuery(con_l, 'SELECT * from RepCalendarAdherence;')
  repAccountCalendarAdherence <- dbGetQuery(con_l, 'SELECT * from RepAccountCalendarAdherence;')
  expect_equal(dim(repDateLocation), c(0,9))
  expect_equal(dim(repDateLocation_l), c(0,13))
  expect_equal(dim(repDateFacility), c(0,9))
  expect_equal(dim(repDateFacility_l), c(0,13))
  expect_equal(dim(accountDateLocationScores), c(0,13))
  expect_equal(dim(accountDateLocation), c(0,7))
  expect_equal(dim(accountDateLocation_l), c(0,11))
  expect_equal(dim(repCalendarAdherence), c(0,10))
  expect_equal(dim(repAccountCalendarAdherence), c(0,11))
  # results should be saved to new tables
  repDateLocation_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateLocation_nightly_newCluster;')
  repDateLocation_l_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateLocation_manual_newCluster;')
  repDateFacility_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateFacility_nightly_newCluster;')
  repDateFacility_l_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateFacility_manual_newCluster;')
  accountDateLocationScores_new <- dbGetQuery(con_l, 'SELECT * from paraRun_AccountDateLocationScores_newCluster;')
  accountDateLocation_new <- dbGetQuery(con_l, 'SELECT * from paraRun_AccountDateLocation_nightly_newCluster;')
  accountDateLocation_l_new <- dbGetQuery(con_l, 'SELECT * from paraRun_AccountDateLocation_manual_newCluster;')
  repCalendarAdherence_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepCalendarAdherence_newCluster;')
  repAccountCalendarAdherence_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepAccountCalendarAdherence_newCluster;')
  expect_equal(dim(repDateLocation_new), c(15,9))
  expect_equal(dim(repDateLocation_l_new), c(0,13))
  expect_equal(dim(repDateFacility_new), c(382,9))
  expect_equal(dim(repDateFacility_l_new), c(0,13))
  expect_equal(dim(accountDateLocationScores_new), c(3661,13))
  expect_equal(dim(accountDateLocation_new), c(1861,7))
  expect_equal(dim(accountDateLocation_l_new), c(0,11))
  expect_equal(dim(repCalendarAdherence_new), c(2,10))
  expect_equal(dim(repAccountCalendarAdherence_new), c(8,11))
  # check whether archiving is done
  repDateLocation_arc_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateLocation_nightly_newCluster_arc;')
  repDateFacility_arc_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateFacility_nightly_newCluster_arc;')
  expect_equal(dim(repDateLocation_arc_new), c(15,10))
  expect_equal(dim(repDateFacility_arc_new), c(382,10))
  expect_equal(repDateLocation_arc_new[, names(repDateLocation_arc_new)!="arcDate"], repDateLocation_new)
  expect_equal(repDateFacility_arc_new[, names(repDateFacility_arc_new)!="arcDate"], repDateFacility_new)
  # disconnect db
  dbDisconnect(con_l)
  dbDisconnect(con)
})

# test for manual run
test_that('test for manual running', {
  # run script
  isNightly <- FALSE
  buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
  runAnchorDriverMockDataSetup(isNightly, buildUID)
  runParaRunDriverForAnchor(isNightly, buildUID)

  # test whether writes to correct output tables
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  # old table should not have any value
  repDateLocation <- dbGetQuery(con, 'SELECT * from RepDateLocation;')
  repDateLocation_l <- dbGetQuery(con_l, 'SELECT * from RepDateLocation;')
  repDateFacility <- dbGetQuery(con, 'SELECT * from RepDateFacility;')
  repDateFacility_l <- dbGetQuery(con_l, 'SELECT * from RepDateFacility;')
  accountDateLocationScores <- dbGetQuery(con_l, 'SELECT * from AccountDateLocationScores;')
  accountDateLocation <- dbGetQuery(con, 'SELECT * from AccountDateLocation;')
  accountDateLocation_l <- dbGetQuery(con_l, 'SELECT * from AccountDateLocation;')
  repCalendarAdherence <- dbGetQuery(con_l, 'SELECT * from RepCalendarAdherence;')
  repAccountCalendarAdherence <- dbGetQuery(con_l, 'SELECT * from RepAccountCalendarAdherence;')
  expect_equal(dim(repDateLocation), c(0,9))
  expect_equal(dim(repDateLocation_l), c(0,13))
  expect_equal(dim(repDateFacility), c(0,9))
  expect_equal(dim(repDateFacility_l), c(0,13))
  expect_equal(dim(accountDateLocationScores), c(0,13))
  expect_equal(dim(accountDateLocation), c(0,7))
  expect_equal(dim(accountDateLocation_l), c(0,11))
  expect_equal(dim(repCalendarAdherence), c(0,10))
  expect_equal(dim(repAccountCalendarAdherence), c(0,11))
  # results should be saved to new tables
  repDateLocation_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateLocation_nightly_newCluster;')
  repDateLocation_l_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateLocation_manual_newCluster;')
  repDateFacility_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateFacility_nightly_newCluster;')
  repDateFacility_l_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepDateFacility_manual_newCluster;')
  accountDateLocationScores_new <- dbGetQuery(con_l, 'SELECT * from paraRun_AccountDateLocationScores_newCluster;')
  accountDateLocation_new <- dbGetQuery(con_l, 'SELECT * from paraRun_AccountDateLocation_nightly_newCluster;')
  accountDateLocation_l_new <- dbGetQuery(con_l, 'SELECT * from paraRun_AccountDateLocation_manual_newCluster;')
  repCalendarAdherence_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepCalendarAdherence_newCluster;')
  repAccountCalendarAdherence_new <- dbGetQuery(con_l, 'SELECT * from paraRun_RepAccountCalendarAdherence_newCluster;')
  expect_equal(dim(repDateLocation_new), c(0,9))
  expect_equal(dim(repDateLocation_l_new), c(15,13))
  expect_equal(dim(repDateFacility_new), c(0,9))
  expect_equal(dim(repDateFacility_l_new), c(456,13))
  expect_equal(dim(accountDateLocationScores_new), c(3661,13))
  expect_equal(dim(accountDateLocation_new), c(0,7))
  expect_equal(dim(accountDateLocation_l_new), c(1861,11))
  expect_equal(dim(repCalendarAdherence_new), c(2,10))
  expect_equal(dim(repAccountCalendarAdherence_new), c(8,11))
  # disconnect db
  dbDisconnect(con_l)
  dbDisconnect(con)
})
