##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: save the output accuracy and importance file
##              for MSO build
##
## created by : marc.cohen@aktana.com
## updated by : shirley.xu@aktana.com
##
## created on : 2015-11-03
## updated on : 2018-09-17
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################
library(data.table)
library(RMySQL)
library(openxlsx)
library(uuid)
library(futile.logger)

saveBuildModelExcel <- function(homedir, WORKBOOK, spreadsheetName, models, output, APpredictors, pName, messages, messageSet, messageSetMessage, messageTopic, expiredMessages, accountPredictorNames, emailTopicNameMap, predictorNamesAPColMap) {
  
  flog.info("Starting processing accuracy and importance file")
  
  # convert ... to ___ in modelNames
  colnames(output) <- gsub('___', '...', colnames(output), fixed=TRUE)
  models$targetOri <- models$target
  models$target <- gsub('___', '...', models$target, fixed=TRUE)
  
  emailTopicNameMap <- list2env(emailTopicNameMap)  # convert to hash for faster access via emailTopicNameMap[["emailTopicId"]]
  numAPpredictors <- APpredictors[APpredictors$isNum==1,c("namesGroup")]
  predictorNamesAPColMap <- list2env(predictorNamesAPColMap) # convert to hash for faster access

  # load messageSet info and merged to messageSet
  flog.info("getting MessageSet info for adding predictor and target related info to importance and accuracy file")
  messageSetMessage <- merge(messageSetMessage, messageSet, by="messageSetId")
  messageSetMessage <- merge(messageSetMessage, messages[,c("messageId","lastPhysicalMessageUID")], by="messageId")
  setindex(messageSetMessage,"lastPhysicalMessageUID")
  # func for checking msg expired status
  checkExpire <- function(msgUID, expiredMessages) {
    if (msgUID %in% expiredMessages) {
      return(TRUE)
    } else {
      return(FALSE)
    }
  }
  # load Account/AccountProduct names mapping from its column names using API
  flog.info("Finish modeling all messages, start processing output files")
  flog.info("load Account/AccountProduct names mapping from its column names using API")
  source(sprintf("%s/common/APICall/global.R",homedir))   # source API call related scripts
  apisecret <- data.table(dbGetQuery(con_l,"SELECT value FROM `sysParameters` WHERE name='apitokenSecret';"))$value[1]
  apiurl <- fromJSON(sprintf("%s/common/APICall/env.json", homedir))
  newAPIConfig <- list("learningApi"=apiurl,"secret"=apisecret, "username"="aktanaadmin", "password"="")
  init(TRUE, newAPIConfig)
  Authenticate(glblLearningAPIurl)    # Authenticate and get a token
  accountAccountProductMap <- getAccountAccountProductIdNameMap()
  
  getAccountAccountProductNameForDisplay <- function(colName, accountAccountProductMap, predictorNamesAPColMap) {
      colName <- getNewNameFromMap(colName, predictorNamesAPColMap)  # colnames in Account/AccountProduct table
      mapNames <- accountAccountProductMap[[colName]] # check provided map
      namesGroupM <- ifelse(is.null(mapNames), colName, mapNames) # get predictor names to be displayed on UI
      return (namesGroupM)
  }

  getNewNameFromMap <- function(currentName, nameMap) {
    mapName <- nameMap[[currentName]] # check provided map
    newName <- ifelse(is.null(mapName), currentName, mapName) # get predictor names to be displayed on UI
    return (newName)
  }
  
  # func for checking msg name
  checkMessageName <- function(msgUID, messages) {
    # check with name is messageTopic first
    messageName <- messageTopic[physicalMessageUID==msgUID, documentDescription]
    temp <- length(messageName)
    if (temp == 0 ) { # if not found, check with messages again
      messageName <- messages[lastPhysicalMessageUID==msgUID, messageName]
    }
    temp <- length(messageName)
    if (temp == 0 ) {
      messageName <- as.character(NA)
    } else if ( temp > 1) {
      messageName <- messageName[1]
    }
    return (messageName)
  }
  # func for checking msg setUID
  checkMessageSetUID <- function(msgUID, messageSetMessage) {
    messageSetUID <- messageSetMessage[lastPhysicalMessageUID==msgUID,externalId]
    temp <- length(messageSetUID)
    if (temp == 0) { 
      messageSetUID <- as.character(NA) 
    } else if (temp>1) { 
      messageSetUID <- paste(messageSetUID,collapse=';') 
    }
    return (messageSetUID)
  }
  # func for adding expiredStatus, messageSetUID to accuracy file
  addInfoToAccuracy <- function(msgStr) {
    msgUID <- substr(msgStr,8,nchar(msgStr))
    expiredStatus <- checkExpire(msgUID, expiredMessages)
    messageSetUID <- checkMessageSetUID(msgUID, messageSetMessage)
    messageName <- checkMessageName(msgUID, messages)
    return (list(expiredStatus, messageName, messageSetUID))
  }
  # func for adding expiredStatus, messageName and predictorType
  addInfoToImportance <- function(predictorNames) {
    namesM <- predictorNames
    if (startsWith(predictorNames,"OPEN___") | startsWith(predictorNames,"SEND___") | startsWith(predictorNames,"CLIC___")) {
      predictorType <- "Message"
      msgUID <- substr(predictorNames,8,nchar(predictorNames))
      expiredStatus <- checkExpire(msgUID, expiredMessages)
      messageName <- checkMessageName(msgUID, messages)
      namesGroup <- msgUID   # namesGroup is for aggregated importance aggregationx
      namesGroupM <- namesGroup # namesGroup is for aggregated importance names display
      namesM <- gsub("___", "...", predictorNames, fixed=TRUE)
    } else if (startsWith(predictorNames,"RTE_")) {
      predictorType <- "EmailTopic"
      expiredStatus <- NA
      messageName <- as.character(NA)
      predictorNameSplit <- strsplit(predictorNames,"_")[[1]]
      namesGroup <- predictorNameSplit[3] # namesGroup is for aggregated importance aggregation
      namesGroupM <- emailTopicNameMap[[namesGroup]] # check provided map for emailTopic Name as namesGroup for aggregated names display
      namesM <- sprintf("%s <%s>: %s", predictorNameSplit[2], namesGroupM, predictorNameSplit[4])
    } else if (startsWith(predictorNames,"priorVisit_")) {
      predictorType <- "Visit"
      expiredStatus <- NA
      messageName <- as.character(NA)
      namesGroup <- "Prior Visit"   # namesGroup is for aggregated importance aggregation
      namesGroupM <- namesGroup # namesGroup is for aggregated importance names display
    } else {
      expiredStatus <- NA
      messageName <- as.character(NA)
      # map back predictorNames to its original column name format e.g.from LoTProxy_akt__2_07_2_49_ to LoTProxy_akt_(2_07,2_49]
      oriPredictorNames <- getNewNameFromMap(predictorNames, predictorNamesAPColMap)
      ind <- regexpr("_akt",oriPredictorNames)
      namesGroup <- substr(oriPredictorNames,1,ind+3)   # namesGroup is for aggregated importance
      colName <- namesGroup  # colnames in Account/AccountProduct table
      if (colName %in% accountPredictorNames) { # further differentiate predictors from Account/AccountProduct
        predictorType <- "Account"
      } else {
        predictorType <- "AccountProduct"
      }
      namesGroupM <- getNewNameFromMap(colName,accountAccountProductMap) # get predictor names to be displayed on UI
      predictorValue <- substr(oriPredictorNames,ind+5,nchar(oriPredictorNames))
      if (colName %in% numAPpredictors) { 
        predictorValue <- gsub("_",".", predictorValue, fixed=TRUE)
      } else {
        predictorValue <- gsub("_"," ", predictorValue, fixed=TRUE)
      }
      namesM <- paste(namesGroupM, predictorValue, sep=": ")
    }
    return (list(namesM, predictorType, expiredStatus, messageName, namesGroup, namesGroupM))
  }
  # add info
  flog.info("Adding predictor and target related info to importance and accuracy file")
  setnames(output, "names", "namesOri")
  output[,c("names","predictorType", "expiredStatus", "messageName","namesGroup","namesGroupM"):=addInfoToImportance(namesOri), by=1:nrow(output)]
  models[,c("expiredStatus", "messageName", "messageSetUID"):=addInfoToAccuracy(targetOri), by=1:nrow(models)]
  
  # aggregate importance based on namesGroup for importance
  # add sum for the group (remove na)
  sumNaRm <- function(x) { return (sum(x, na.rm=TRUE))}
  aggOutput1 <- output[,-c("namesOri","expiredStatus","messageName","names","namesGroupM")][, lapply(.SD, sumNaRm),by=c("namesGroup","predictorType")]
  # get names, predictorType, expiredStatus, messageName
  getGroupInfo <- function(x) { return (x[1])}
  aggOutput2 <- output[,c("predictorType","expiredStatus","messageName","namesGroup","namesGroupM")][, lapply(.SD, getGroupInfo),by=c("namesGroup","predictorType")]
  # get non-expired message count
  countNaRm <- function(x) { return (sum(!is.na(x)))}
  aggOutput3 <- output[,-c("namesOri","expiredStatus","messageName","names","namesGroupM")][, lapply(.SD, countNaRm),by=c("namesGroup","predictorType")]
  colnames(aggOutput3) <- paste("count_",colnames(aggOutput3),sep="")
  # sumup result
  aggOutput <- cbind(aggOutput1,aggOutput2[,-c("namesGroup","predictorType")],aggOutput3[,-c("count_namesGroup","count_predictorType")])
  
  # find the dropped predictors in accountProduct
  droppedPredictors <- data.table(APpredictors[!(APpredictors$namesGroup %in% aggOutput$namesGroup),c("namesGroup","dropReason")])
  if (dim(droppedPredictors)[1]>0) {
    droppedPredictors[is.na(droppedPredictors$dropReason),c("dropReason")] <- "<2 unique" # replace dropReason for predictors dropped in loop to <2 unique
    droppedPredictors$predictorType <- "AccountProduct"
    droppedPredictors[droppedPredictors$namesGroup %in% accountPredictorNames, c("predictorType")] <- "Account"  # add predictorType
    droppedPredictors[,namesGroupM:=mapply(getNewNameFromMap,namesGroup,MoreArgs=list(nameMap=accountAccountProductMap))]   # map names as shown in API response if available
    aggOutput <- rbind(aggOutput, droppedPredictors, fill=TRUE, stringsAsFactors=FALSE) 
  }
  
  # clean
  rm(aggOutput1,aggOutput2,aggOutput3)
  aggOutput$namesGroup <- NULL
  output[,c("namesGroup","namesGroupM")] <- NULL
  setnames(aggOutput, "namesGroupM", "names")
  
  # add to excel file
  addWorksheet(WORKBOOK,pName)                           # save results to the xlsx file
  writeData(WORKBOOK,pName,output,startRow=1)
  
  tName <- sprintf("%s_reference",pName)
  addWorksheet(WORKBOOK,tName)
  writeData(WORKBOOK,tName,models,startRow=1)
  
  tName <- sprintf("%s_aggImportance",pName)
  addWorksheet(WORKBOOK,tName)                        
  writeData(WORKBOOK,tName,aggOutput,startRow=1)
  
  # save excel output
  flog.info("Save workbook")
  
  statusCode <- 0
  statusCode <- tryCatch(saveWorkbook(WORKBOOK, file = spreadsheetName, overwrite = T), 
                         error = function(e) {
                           flog.info("Original error message: %s", e)
                           flog.error('Error in saveWorkbook',name='error')
                           return (NA)
                         })
  
  if (is.na(statusCode)) {
    stop("failed to save output to excel file")
  }
  flog.info("finish saving MSO build resulting accuracy and importance file， return from saveBuildModelExcel")
}

saveBuildModelDB <- function(con_l, BUILD_UID, RUN_UID, logName, printName, spreadsheetName) {
  
  flog.info('save output files to DB')
  
  blobIt <- function(name)
  {
    t0 <- readBin(name,what="raw",n=1e7)
    t1 <- paste0(t0,collapse="")
    return(t1)
  }
  
  # save model build results in the learning DB tables
  # update the learning DB File table
  outfile <- blobIt(logName)
  SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"log","txt",object.size(outfile),outfile)
  dbGetQuery(con_l,SQL)
  
  outfile <- blobIt(printName)
  SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"print","txt",object.size(outfile),outfile)
  dbGetQuery(con_l,SQL)
  
  importance <- read.xlsx(spreadsheetName,sheet=2)
  tempName1 <- tempfile(fileext=".csv")
  write.csv(importance,file=tempName1)
  outfile <- blobIt(tempName1)
  SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"importance","csv",object.size(outfile),outfile)
  dbGetQuery(con_l,SQL)
  
  accuracy <- read.xlsx(spreadsheetName,sheet=3)
  write.csv(accuracy,file=tempName1)
  outfile <- blobIt(tempName1)
  SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"accuracy","csv",object.size(outfile),outfile)
  dbGetQuery(con_l,SQL)
  
  aggImportance <- read.xlsx(spreadsheetName,sheet=4)
  write.csv(aggImportance,file=tempName1)
  outfile <- blobIt(tempName1)
  SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"aggImportance","csv",object.size(outfile),outfile)
  dbGetQuery(con_l,SQL)
  
  flog.info('finish saving outfiles to LearningFile, return from saveBuildModelDB')
}
