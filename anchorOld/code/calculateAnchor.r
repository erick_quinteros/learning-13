##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: estimate anchor locations for reps
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-27
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
#
#
#  Assumes the following tables: interactions is global and has fields: repId, latitude, longitude
#
#
#

calculateAnchor <- function()
    {
        library(data.table)
        library(markovchain)
        library(Rsolnp)

        offLoopSize <- 4

        flog.info(as.character(as.list(sys.call())[[1]]))

        interactions <- interactions[!is.na(latitude)]
        interactions[,c("aveLat","aveLong"):=list(mean(latitude),mean(longitude)),by=list(repId,date)]

        result <- data.table()

        for(i in sort(unique(interactions$repId))){

            # loop for each rep
            x <- interactions[repId==i & !is.na(aveLat),list(aveLong,aveLat,date)]
            setkey(x,date)
            x <- unique(x)

            y <- copy(x)
            y[,date:=NULL]
            y <- unique(y)
            centers <- min(15,trunc(dim(y)[1]/3))
            y$state <- ifelse(centers>2,kmeans(y[,1:2,with=FALSE],centers=centers),data.table(cluster=1))

            setkey(y,aveLat,aveLong)
            setkey(x,aveLat,aveLong)
            x <- merge(x,y,all.x=TRUE)

            setkey(x,date)
            x <- unique(x)

            plotData(sprintf("rep=%s clusters=%s",i,centers),x)

            off <- 0
            if(dim(x)[1]>2)
                {
                    fit0 = fit2 = ""
                    fit2 <- tryCatch({fitHigherOrder(x$state,order=3)},warning=function(w){flog.warn("Fit2 Warning %s rep %s",w,i)},error=function(e){flog.error("Fit2 Error %s rep %s",e,i)})

                    n <- x[,.I[.N]]
                    n <- x[seq(n,n-2,-1)]$state

                    if(class(fit2)=="list")
                        {
                            lambda <- fit2$lambda
                            if(lambda[1]<.95)
                                {
                                    Q1 <- as.matrix(lambda[1]*fit2$Q[[1]])
                                    Q2 <- as.matrix(lambda[2]*fit2$Q[[2]])
                                    Q3 <- as.matrix(lambda[3]*fit2$Q[[3]])

                                    for(off in 0:offLoopSize)
                                        {
                                            d <- dim(Q1)[1]
                                            v1 <- rep(0,1,d)
                                            v1[which(colnames(Q1)==n[1])] <- 1
                                            v2 <- rep(0,1,d)
                                            v2[which(colnames(Q1)==n[2])] <- 1
                                            v3 <- rep(0,1,d)
                                            v3[which(colnames(Q1)==n[3])] <- 1

                                            TM <- Q1%*%v1 + Q2%*%v2 + Q3%*%v3

                                            s <- colnames(Q1)[which(TM==max(TM))[1]]

                                            latlong <- getLocation(s,x)
                                            result <- rbind(result,data.table(repId=i,offset=off,latitude=latlong["lat"],longitude=latlong["long"],method=sprintf("MC_O3 lambda: %.3f %.3f %.3f",lambda[1],lambda[2],lambda[3])))
                                            n[3] <- n[2]
                                            n[2] <- n[1]
                                            n[1] <- s
                                        }
                                    next
                                }
                        }
                     fit0 <- tryCatch({markovchainFit(data=x$state)},warning=function(w){flog.warn("Fit0 Warning %s rep %s",w,i)},error=function(e){flog.error("Fit0 Error %s rep %s",e,i)})
                     if(class(fit0)=="list"){
                            TM <- as.data.table(slot(fit0$estimate,"transitionMatrix"))
                            TM$last <- colnames(TM)
                            TM[,max:=which.max(.SD),by=last]
                            nxt <- colnames(TM)[TM$max]
                            TM$nxt <- nxt
                            s <- TM[last==n[1]]$nxt
                            TMlength <- dim(TM)[1]
                            for(off in 0:offLoopSize)
                                {
                                  latlong <- getLocation(s,x)
                                  result <- rbind(result,data.table(repId=i,offset=off,latitude=latlong["lat"],longitude=latlong["long"],method="MC"))
                                  p=TM[last==s,1:TMlength,with=FALSE]
                                  if(sum(p)>0)s <- colnames(TM)[sample(1:TMlength,1,prob=p)]
                                  else s <- colnames(TM)[sample(1:TMlength,1)]
                                }
                             next
                        }
                     }
            else {
                    for(off in 0:offLoopSize)result <- rbind(result,data.table(repId=i,offset=off,latitude=mean(x$aveLat),longitude=mean(x$aveLong),method="Mean"))
                }
        }
        return(result)
    }

#
# convert state coding into latitude and longitude
#
getLocation <- function(s,x)
    {
        lat <- mean(x[state==s]$aveLat)
        long <- mean(x[state==s]$aveLong)
        return(list(lat=lat,long=long))
    }

#
## plotting function
#
plotData <- function(title,x)
    {
        library(lattice)
        print(xyplot(aveLat~aveLong,group=state,x,type=c('p'),cex=1,main=title,pch=20))
        print(xyplot(aveLat~aveLong,x,type=c('p','l'),cex=.8,main=title,pch=20))
        x[,c("lat","long"):=list(mean(aveLat),mean(aveLong)),by=state]
        print(xyplot(lat~long,x,type=c('p','l'),cex=1,main=title,pch=20))
        x[,c("from","to"):=list(state,state)]
        x$to <- x[2:(dim(x)[1]-1)]$to
        x$ctr <- 1
        x[,thickness:=sum(ctr),by=list(from,to)]
        lw <- 1:(max(x$thickness))
        print(xyplot(lat~long,x,pch=20,type=c('p','l'),group=thickness,lwd=lw,main=title))
    }
