#!/bin/bash
#
# aktana-learning Install script for Aktana Learning Engines.
#
# description: Learning modules are written in R. The install script is used
#              to install the appropriate R packages required for the module
#
# created by : satya.dhanushkodi@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
MODULE=messageSequence
RSCRIPT=messageSequenceDriver
PRG="$0"
LEARNING_HOME=`dirname "$PRG"`/..
export LEARNING_HOME
BIN_DIR=$LEARNING_HOME/bin
ENV_NAME="$1"

if [ -z "$1" ] ; then
   echo "Error: Specify Environment to run on";
   exit 1
fi

echo "Run on Environment " $1

if [ $ENV_NAME == 'pfizerusdev' ]; then
  DB_HOST=pfizerusrds.aktana.com
  DB_NAME=pfizerusdev
  DB_USER=pfizerusadmin
  DB_PASSWORD=njTB95MacVDLaMUAnKHdXubOkVPyCU
  RUN_DATE=2015-01-01
  BUILD_UID=9ccccdae-a950-4f07-a99c-0b1727245521
  CUSTOMER=pfizerus
fi

echo "Module " $MODULE
echo "HOST " $DB_HOST
echo "NAME " $DB_NAME
echo "USER " $DB_USER
echo "HOST " $DB_HOST
echo "Module " $MODULE
echo "Rscript " $RSCRIPT
echo "Build_uid " $BUILD_UID
echo "Config_uid " $CONFIG_UID

$BIN_DIR/runModule.sh -m $MODULE -r $RSCRIPT -h $DB_HOST -s $DB_NAME -u $DB_USER -p $DB_PASSWORD -d $RUN_MODEL -c $CUSTOMER -e $ENV_NAME -b $BUILD_UID - g $CONFIG_UID
