context('testing anchorDriver script in ANCHOR module')
print(Sys.time())

############################# func for run anchorDriver setup #############################################
runAnchorDriverMockDataSetup <- function (isNightly,buildUID) {
  # load library and source script
  library(Learning)
  library(Learning.DataAccessLayer)
  library(uuid)
  source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
  source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
  source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('RepDateLocation','RepDateFacility'),pfizerusdev_learning=c('RepDateLocation','RepDateFacility','LearningRun','LearningBuild'))
  # learningBuild not required for run engagementDriver, just for testing
  requiredMockDataList_module <- list(pfizerusdev_learning=c('AccountDateLocation','AccountDateLocationScores','RepCalendarAdherence','RepAccountCalendarAdherence'), pfizerusdev=c('Interaction','Facility','Rep','InteractionType','InteractionAccount','RepTeamRep','AccountDateLocation','Account'),pfizerusdev_stage=c('RepAccountAssignment_arc'),pfizerusdev_archive=c('STG_Call2_vod__c_H'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')
  # set up unit test build folder and related configs
  # get buildUID
  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  if (!isNightly) {
    setupMockBuildDir(homedir, 'anchor', buildUID, files_to_copy='learning.properties', forNightly=FALSE)
  } else {
    setupMockBuildDir(homedir, 'anchor', buildUID, files_to_copy=NULL, forNightly=TRUE)
  }
}

############################### func for run anchorDriver ########################################
runAnchorDriver <- function (isNightly, buildUID) {
  if (!isNightly) {
    # manual key in rundate
    # rundate <<- readModuleConfig(homedir, 'anchor','rundate')
    # parameters needed for manual running
    RUN_UID <<- UUIDgenerate()
    BUILD_UID <<- buildUID
    config <- initializeConfigurationNew(homedir, BUILD_UID)
    versionUID <- config[["versionUID"]]
    configUID  <- config[["configUID"]]
    con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',0,'ANCHOR','running','%s');",RUN_UID,buildUID,versionUID,configUID,now)

    dbClearResult(dbSendQuery(con_l, SQL))
    dbDisconnect(con_l)
  } else {
    if (exists("BUILD_UID", envir=globalenv())) {rm(BUILD_UID, envir=globalenv())}
    if (exists("RUN_UID", envir=globalenv())) {rm(RUN_UID, envir=globalenv())}
  }
  # run engagementDriver script
  debugMode <<- TRUE
  set.seed(1)  # fix that random sampling
  # unload pacakges import by unit test & use the one just the script itself
  detach("package:Learning")
  detach("package:Learning.DataAccessLayer")
  detach("package:RMySQL")
  detach("package:futile.logger")
  detach("package:openxlsx")
  detach("package:properties")
  detach("package:uuid")
  source(sprintf("%s/anchor/anchorDriver.R",homedir))
  config <- initializeConfigurationNew(homedir, BUILD_UID)
  predictMode <- getConfigurationValueNew(config,"LE_AN_predictMode")
  return(list(RUN_UID,predictMode))
}

############################# func for testing log ########################################################

testLog <- function(log_contents, isNightly, predictMode, calculate_adl, calculate_calAd, runUID) {
  facilityMode = FALSE
  if(predictMode == "Facility") facilityMode = TRUE

  # test log output from anchorDriver
  ind <- grep("Run initialized at", log_contents, calculate_adl)
  if(isNightly) expect_str_pattern_find(log_contents[ind+2], "Running Anchor nightly")
  if(!isNightly) expect_str_pattern_find(log_contents[ind+2], "Running Anchor manually")
  expect_str_end_match(log_contents[ind+2], "predictRundate=2017-11-01")
  if(calculate_adl) {  # whether run recalculate accountDateLocations or not
    expect_str_start_match(log_contents[ind+4], "Recalculate")
  } else {
    expect_str_start_match(log_contents[ind+4], "Not recalculate")
    expect_str_end_match(log_contents[ind+4], "RUN_UID") #dummy, same as stored data
  }
  if(calculate_calAd) {  # whether run recalculate accountDateLocations or not
    expect_str_start_match(log_contents[ind+6], "Recalculate")
  } else {
    expect_str_start_match(log_contents[ind+6], "Not recalculate")
    expect_str_end_match(log_contents[ind+6], "RUN_UID") #dummy, same as stored data
  }
  expect_str_end_match(log_contents[ind+7], "5: predict for 2017-11-01, 2017-11-02, 2017-11-03, 2017-11-06, 2017-11-07, 2017-11-08")

  # test log output from loadAnchorData
  ind <- grep("Start LoadAnchorData", log_contents)
  expect_str_end_match(log_contents[ind+1], "(6230,9)")  # dim of all Interaction Data
  expect_str_end_match(log_contents[ind+2], "(3,2)")  # dim of validReps
  expect_str_end_match(log_contents[ind+3], "(1012,2)")  # dim of repAccountAssignment
  expect_str_end_match(log_contents[ind+4], "(0,2)")  # dim of repTeamRep
  if(calculate_adl) {
    expect_str_end_match(log_contents[ind+5], "(5343,6)")  # dim of interactions used to recalculate accountDateLocationScores
  } else {
    expect_str_end_match(log_contents[ind+5], "(3661,8)")  # dim of saved accountDateLocationScores
  }
  expect_str_end_match(log_contents[ind+6], "(5529,7)")  # dim of filtered interaction
  expect_str_end_match(log_contents[ind+7], "(46,7)")  # dim of future(planned visit) interactions
  expect_str_end_match(log_contents[ind+8], "(4692,6)")  # dim of history interaction
  expect_str_end_match(log_contents[ind+9], "0")  # length of newRepList
  if (calendarAdherenceOn) {
    if (calculate_calAd) {
      expect_str_end_match(log_contents[ind+11], "(25,6)")  # dim of callHistory
    } else {
      expect_str_end_match(log_contents[ind+10], "(2,5)")  # dim of repCalendarAdherence
      expect_str_end_match(log_contents[ind+11], "(8,6)")  # dim of repAccountCalendarAdherence
    }
  }

  # test log output from calculateAnchor
  ind <-  grep("Start calculateAnchor:", log_contents)
  expect_str_end_match(log_contents[ind+3], "adding 1 new reps defined as interactions<=40 to newRepList")
  expect_str_end_match(log_contents[ind+4], "Predict for 2/3 Reps (new rep deined as <= 40 ints record)")
  expect_str_end_match(log_contents[grep("Return from calculateAnchor:", log_contents)], "dim(result)=(570,12), nrow(newRepList)=1")  # dimension of result

  # test log output from calculateAccountDateLocationScores
  if(calculate_adl) {
    ind <-  grep("Start calculateAccountDateLocationScores:", log_contents)
    expect_str_end_match(log_contents[ind], "5343 rows of interaction data")  # input data dimension
    expect_str_end_match(log_contents[ind+11], "(3661,8)")  # dimension of result
  }

  # test log output from calculateAccountDateLocation
  ind <- grep("Start calculateAccountDateLocation:",  log_contents)
  expect_str_end_match(log_contents[ind], "3661 rows of AccountDateLocationScores data")  # input data dimension
  expect_str_end_match(log_contents[ind+1], "(1922,6)")  # dimension of result


  # test log output from calculateAnchorForNewRep
  ind <-  grep("Start calculateAnchorForNewRep:", log_contents)
  expect_str_end_match(log_contents[ind], "for 1 reps")
  if (isNightly) {
    expect_str_end_match(log_contents[grep("Return from calculateAnchorForNewRep:", log_contents)], "add newRepResult(dim=(317,12)) to anchorResult")  # dimension of result
  } else {
    expect_str_end_match(log_contents[grep("Return from calculateAnchorForNewRep:", log_contents)], "add newRepResult(dim=(312,12)) to anchorResult")  # dimension of result
  }

  # test log output from processAnchorResult
  ind <- grep("Start processAnchorResult", log_contents)
  if (isNightly) {
    expect_str_end_match(log_contents[ind], "dim=(887,12)") # input data dimension
    expect_str_end_match(log_contents[ind+2], "dim=(887,12) using Centroid mode") # input data dimension
    expect_str_end_match(log_contents[ind+3], "dim(resultProcessed)=(15,9)") # dimension of processed result from centroid mode
    if(facilityMode) {
      expect_str_end_match(log_contents[ind+4], "adding processed result from predictMode=Facility")
      expect_str_end_match(log_contents[ind+5], "dim=(887,12) using Facility mode")  # input data dimension
      expect_str_end_match(log_contents[ind+6], "dim(resultProcessed)=(385,9)")  # dimension of processed result from centroid mode
      expect_str_end_match(log_contents[ind+7], "dim(resultProcessedCentroid)=(15,9), dim(resultProcessedFacility)=(385,9)")  # dimension of final processed result
    } else{
      expect_str_end_match(log_contents[ind+5], "dim(resultProcessedCentroid)=(15,9), dim(resultProcessedFacility)=(0)")
    }
  } else {
    expect_str_end_match(log_contents[ind], "dim=(882,12)")
    expect_str_end_match(log_contents[ind+2], "dim=(882,12) using Centroid mode")
    expect_str_end_match(log_contents[ind+3], "dim(resultProcessed)=(15,9)")
    if(facilityMode) {
      expect_str_end_match(log_contents[ind+4], "adding processed result from predictMode=Facility")
      expect_str_end_match(log_contents[ind+5], "dim=(882,12) using Facility mode")
      expect_str_end_match(log_contents[ind+6], "dim(resultProcessed)=(459,9)")
      expect_str_end_match(log_contents[ind+7], "dim(resultProcessedCentroid)=(15,9), dim(resultProcessedFacility)=(459,9)")  # dimension of final processed result
    } else{
      expect_str_end_match(log_contents[ind+5], "dim(resultProcessedCentroid)=(15,9), dim(resultProcessedFacility)=(0)")
    }
  }

  # test log output from saveAnchorResults
  ind <- grep("Enter Save Anchor Result", log_contents)
  if (isNightly) {
    expect_str_end_match(log_contents[ind+1], "save 15 rows of RepDateLocation to DSE")
    if(facilityMode) expect_str_end_match(log_contents[ind+2], "save 385 rows of RepDateFacility to DSE")
  } else {
    expect_str_end_match(log_contents[ind+1], "save 15 rows of RepDateLocation to Learning")
    if(facilityMode) expect_str_end_match(log_contents[ind+2], "save 459 rows of RepDateFacility to Learning")
  }
  if(calculate_adl) {
    ind <- grep("Enter saveAccountDateLocationScores Result", log_contents)
    expect_str_end_match(log_contents[ind+1], "save 3661 row of AccountDateLocationScores to Learning")
  }

  # test log output from processAccountDateLocationResult
  ind <- grep("Start processAccountDateLocationResult", log_contents)
  expect_str_end_match(log_contents[ind], "dim=(1922,6)") # dimension of input data
  expect_str_end_match(log_contents[ind+1], "(1861,7)") # dimension of output data

  # test log output from saveAccountDateLocationResult
  ind <- grep("Enter saveAccountDateLocation Result", log_contents)
  if (isNightly) {
    expect_str_end_match(log_contents[ind+1], "save 1861 row of AccountDateLocation to DSE")
  } else {
    expect_str_end_match(log_contents[ind+1], "save 1861 row of AccountDateLocation to Learning")
  }
}

############################# main script for testing ########################################################

test_that('test for nightly running', {
  # run script
  isNightly <- TRUE
  buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
  runAnchorDriverMockDataSetup(isNightly, buildUID)
  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))
  info <- runAnchorDriver(isNightly, buildUID)
  runUID <- info[[1]]
  predictMode <- info[[2]]

  # run test cases
  # test build_dir has the correct structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+2)

  # test entry in DB is fine
  # load data
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  repDateLocation <- data.table(dbGetQuery(con, 'SELECT * from RepDateLocation;'))
  repDateLocation_l <- dbGetQuery(con_l, 'SELECT * from RepDateLocation;')
  repDateFacility <- data.table(dbGetQuery(con, 'SELECT * from RepDateFacility;'))
  repDateFacility_l <- dbGetQuery(con_l, 'SELECT * from RepDateFacility;')
  accountDateLocationScores_new <- data.table(dbGetQuery(con_l, 'SELECT * from AccountDateLocationScores;'))
  accountDateLocation_new_l <- data.table(dbGetQuery(con_l, 'SELECT * from AccountDateLocation;'))
  accountDateLocation_new <- data.table(dbGetQuery(con, 'SELECT * from AccountDateLocation;'))
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')
  dbDisconnect(con_l)
  dbDisconnect(con)
  # check repDateLocation & repDateFacility
  # check dimension
  expect_equal(dim(repDateLocation), c(15,9))
  expect_equal(dim(repDateLocation_l), c(0,13))
  if(predictMode == "Facility") {
    expect_equal(dim(repDateFacility), c(385,9))
  } else {
    expect_equal(dim(repDateFacility), c(0,9))
  }
  expect_equal(dim(repDateFacility_l), c(0,13))
  if(predictMode == "Facility") expect_equal(aggregate(cbind(count=facilityId)~date+repId, data=repDateFacility, FUN = function(x){length(x)})$count,c(15,24,63,51,10,20,7,13,11,14,51,36,46,14,10)) # group by count of results
  # check data same as saved repDateLocation
  # repDateLocation_DSE <- repDateLocation
  # save(repDateLocation_DSE, file = sprintf('%s/anchor/tests/data/from_anchorDriver_repDateLocation_DSE.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_anchorDriver_repDateLocation_DSE.RData', homedir))
  expect_equal(repDateLocation[,-c("learningRunUID","createdAt")], repDateLocation_DSE[,-c("learningRunUID","createdAt")])
  # check data same as saved repDateFacility
  # repDateFacility_DSE <- repDateFacility
  # save(repDateFacility_DSE, file = sprintf('%s/anchor/tests/data/from_anchorDriver_repDateFacility_DSE.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_anchorDriver_repDateFacility_DSE.RData', homedir))
  expect_equal(repDateFacility[,-c("learningRunUID","createdAt")], repDateFacility_DSE[,-c("learningRunUID","createdAt")])

  # check accountDateLocation & accountDateLocationScores
  # check dimension
  expect_equal(dim(accountDateLocationScores_new), c(3661,13))
  expect_equal(dim(accountDateLocation_new_l), c(0,11))
  expect_equal(dim(accountDateLocation_new), c(1861,7))
  # check same as saved accountDateLocationScores
  accountDateLocationScores <- fread(sprintf('%s/anchor/tests/data/pfizerusdev_learning/AccountDateLocationScores.csv',homedir))   # nightly run not update accountDateLocation, it should be the same as mock data
  expect_equal(accountDateLocationScores_new[order(accountId, facilityId, dayOfWeek, periodOfDay),names(accountDateLocationScores),with=F], accountDateLocationScores[order(accountId, facilityId, dayOfWeek, periodOfDay),])
  # check same as saved accountDateLocation
  load(sprintf('%s/anchor/tests/data/from_anchorDriver_accountDateLocation_DSE.RData',homedir))
  expect_equal(accountDateLocation_new[,-c("learningRunUID","createdAt")],accountDateLocation_DSE[,-c("learningRunUID","createdAt")])
  expect_equal(unique(accountDateLocation_new$learningRunUID),runUID)

  # check learning run log entry
  expect_equal(dim(learningRun), c(1,10))
  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(unname(unlist(learningRun[,c('isPublished','runType','executionStatus')])), c(1,'ANCHOR','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)

  # test the structure and contents in log file
  calculate_adl <- FALSE
  calculate_calAd <- FALSE
  log_contents <- readScoreLogFile(homedir,buildUID,runUID)
  testLog(log_contents, isNightly, predictMode, calculate_adl, calculate_calAd, runUID)
})

test_that('test for manual running', {
  # run script
  isNightly <- FALSE
  buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
  runAnchorDriverMockDataSetup(isNightly, buildUID)
  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))
  info <- runAnchorDriver(isNightly, buildUID)
  runUID <- info[[1]]
  predictMode <- info[[2]]

  # run test cases
  # test build_dir has the correct structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,BUILD_UID))
  expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+2)
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,paste("Score",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,BUILD_UID,paste("Score",RUN_UID,sep="_")))

  # test entry in DB is fine
  # load data
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  repDateLocation <- dbGetQuery(con, 'SELECT * from RepDateLocation;')
  repDateLocation_l <- data.table(dbGetQuery(con_l, 'SELECT * from RepDateLocation;'))
  repDateFacility <- dbGetQuery(con, 'SELECT * from RepDateFacility;')
  repDateFacility_l <- data.table(dbGetQuery(con_l, 'SELECT * from RepDateFacility;'))
  accountDateLocationScores_new <- data.table(dbGetQuery(con_l, 'SELECT * from AccountDateLocationScores;'))
  accountDateLocation_new_l <- data.table(dbGetQuery(con_l, 'SELECT * from AccountDateLocation;'))
  accountDateLocation_new <- data.table(dbGetQuery(con, 'SELECT * from AccountDateLocation;'))
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')
  dbDisconnect(con_l)
  dbDisconnect(con)
  # check repDateLocation & repDateFacility
  # check dimension
  expect_equal(dim(repDateLocation), c(0,9))
  expect_equal(dim(repDateLocation_l), c(15,13))
  expect_equal(dim(repDateFacility), c(0,9))
  if(predictMode == "Facility") {
    expect_equal(dim(repDateFacility_l), c(459,13))
  } else {
    expect_equal(dim(repDateFacility_l), c(0,13))
  }
  if(predictMode == "Facility") expect_equal(aggregate(cbind(count=facilityId)~date+repId, data=repDateFacility_l, FUN = function(x){length(x)})$count,c(15,24,63,51,10,20,7,13,11,14,85,47,65,18,16)) # group by count of results
  # check data same as saved repDateLocation
  # repDateLocation_learning <- repDateLocation_l
  # save(repDateLocation_learning, file=sprintf('%s/anchor/tests/data/from_anchorDriver_repDateLocation_learning.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_anchorDriver_repDateLocation_learning.RData', homedir))
  expect_equal(repDateLocation_l[,-c("learningRunUID","createdAt","updatedAt")], repDateLocation_learning[,-c("learningRunUID","createdAt","updatedAt")])
  # check data same as saved repDateFacility
  # repDateFacility_learning <- repDateFacility_l
  # save(repDateFacility_learning, file=sprintf('%s/anchor/tests/data/from_anchorDriver_repDateFacility_learning.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_anchorDriver_repDateFacility_learning.RData', homedir))
  expect_equal(repDateFacility_l[,-c("learningRunUID","createdAt","updatedAt")], repDateFacility_learning[,-c("learningRunUID","createdAt","updatedAt")])
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility.RData', homedir))
  repDateFacility_l$runDate <- as.Date(repDateFacility_l$runDate)
  repDateFacility_l$date <- as.Date(repDateFacility_l$date)
  result_facility[is.na(accountId),accountId:=0] #replace na accountValues
  if(predictMode == "Facility") {
    expect_equal(repDateFacility_l[source != "territory_noEnoughInts",-c("learningBuildUID","learningRunUID","createdAt","updatedAt")][order(repId, date, facilityId),], result_facility[source != "territory_noEnoughInts"][order(repId, date, facilityId),])
  }
  # check accountDateLocation & accountDateLocationScores
  # check dimension
  expect_equal(dim(accountDateLocationScores_new), c(3661,13))
  expect_equal(dim(accountDateLocation_new), c(0,7))
  expect_equal(dim(accountDateLocation_new_l), c(1861,11))
  # check same as saved accountDateLocationScores
  load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocationScores_result.RData',homedir))
  accountDateLocationScores$facilityDoWScore <- as.double(accountDateLocationScores$facilityDoWScore)
  accountDateLocationScores$facilityPoDScore <- as.double(accountDateLocationScores$facilityPoDScore)
  expect_equal(accountDateLocationScores_new[order(accountId, facilityId, dayOfWeek, periodOfDay),names(accountDateLocationScores),with=F], accountDateLocationScores[order(accountId, facilityId, dayOfWeek, periodOfDay),])
  expect_equal(unique(accountDateLocationScores_new$learningRunUID),runUID)
  expect_equal(unique(accountDateLocationScores_new$learningBuildUID),buildUID)
  expect_equal(unique(accountDateLocationScores_new$runDate),as.character(predictRundate))
  # check same as saved accountDateLocation
  load(sprintf('%s/anchor/tests/data/from_anchorDriver_accountDateLocation_l.RData',homedir))
  accountDateLocation_new_l$date <- as.Date(accountDateLocation_new_l$date)
  accountDateLocation_new_l$runDate <- as.Date(accountDateLocation_new_l$runDate)
  expect_equal(accountDateLocation_new_l[order(accountId,facilityId,date),c("accountId","facilityId","latitude","longitude","date","facilityDoWScore","runDate")], accountDateLocation[order(accountId,facilityId,date),c("accountId","facilityId","latitude","longitude","date","facilityDoWScore","runDate")])
  expect_equal(unique(accountDateLocation_new_l$learningRunUID),runUID)
  expect_equal(unique(accountDateLocation_new_l$learningBuildUID),buildUID)

  # check learning run entry
  expect_equal(dim(learningRun), c(1,10))
  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(unname(unlist(learningRun[,c('isPublished','runType','executionStatus')])), c(0,'ANCHOR','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)

  # test the structure and contents in log file
  calculate_adl <- TRUE
  calculate_calAd <- TRUE
  log_contents <- readScoreLogFile(homedir,buildUID,runUID)
  testLog(log_contents, isNightly, predictMode, calculate_adl, calculate_calAd, runUID)
})
